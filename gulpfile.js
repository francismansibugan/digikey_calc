var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    minify = require('gulp-minify');

gulp.task('style', function() {
    gulp.src('source/scss/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./'))
});

gulp.task('style2', function() {
    return gulp.src([
        'source/scss/others/*.css',
        'source/scss/main/**/*.scss'
        ])
        .pipe(concat('main.css'))
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(minify())
        .pipe(gulp.dest('./css/'))
});

gulp.task('scripts', function() {
    return gulp.src([
            'source/js/others/jquery.js',
            'source/js/others/bootstrap.min.js',
            'source/js/main.js'
        ])
        .pipe(concat('main.js'))
        .pipe(minify())
        .pipe(gulp.dest('./js/'));
});

// Watch task
gulp.task('watch',function() {
    gulp.watch('source/scss/**/*.scss',['style','style2']);
    gulp.watch('source/js/**/*.js',['scripts']);
});

gulp.task('default', ['style','style2','scripts','watch']);